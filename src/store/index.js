import Vue from 'vue'
import Vuex from 'vuex'
import request from "@/store/request";
import telegram from "@/store/telegram";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    request, telegram
  }
})
