import axios  from "axios";
export default {
    state: {
        telegram: {}
    },
    mutations: {
        telegramData(state, telegramData) {
            state.telegram = telegramData
        }
    },
    actions: {
        async sendTelegram(dataTelegram) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/telegram/sendTelegram'
            await axios.post(apiURL, dataTelegram).then((res) => {
                if(res.status === 201) {
                    console.log('res++++', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        }
    },
    getters: {
        telegram: u => u.telegram || []
    }
}
