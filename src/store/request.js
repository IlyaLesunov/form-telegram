import axios  from "axios";
export default {
    state: {
        request: {}
    },
    mutations: {
        requestData(state, requestData) {
            state.request = requestData
        }
    },
    actions: {
        async createRequest(dataRequest) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/request/sendRequest'
            await axios.post(apiURL, dataRequest).then((res) => {
                if(res.status === 200) {
                    console.log('res++++', res.data.message )
                }
            }).catch((error) => {
                console.log(error)
            })
        }
    },
    getters: {
        request: u => u.request || []
    }
}
