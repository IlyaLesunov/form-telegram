const express = require('express')
const requestRouter = require('./routers/request.router')
const telegramRouter = require('./routers/telegram.router')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

const PORT = process.env.PORT ||  3000
async function start() {
    try {
        app.listen(PORT, () => {
            console.log(`Порт запущен на ${PORT}`)
        })
    } catch (e) {
        console.log(e)
    }
}

start()

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use(cors(PORT))

app.use('/api/request', requestRouter)
app.use('/api/telegram', telegramRouter)
