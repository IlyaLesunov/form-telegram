const { Router } = require('express')
const router = Router()
const TelegramBot = require('node-telegram-bot-api');

router.post('/sendRequest', async (res, req) => {

    const token = '1864984503:AAESqKOkFTOU-ENuxD2MpNRQE4lwyRPW-G8';
    const bot = new TelegramBot(token, {polling: true});
    console.log('bot', bot)
    const chatID = '714731255'
    if (res.body.title && res.body.subtitle !== '') {
        const requestData = `Новая заявка с сайта Форма: \n\nЗаголовок: ${res.body.title} \n\nКомментарий: ${res.body.subtitle}`
        bot.sendMessage(chatID, `Привет, ${requestData}`).then(() => {
            res.body.title = ''
            res.body.subtitle = ''
            return req.status(200).json({message: 'Ваша заявка отправлена'})
        }).catch((err) => {
            return console.log('err', err)
        });
    } else {
        return req.status(500).json({message: 'Поля не должны быть пустыми'})
    }
})

module.exports = router
