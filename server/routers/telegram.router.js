const { Router } = require('express')
const router = Router()
const TelegramBot = require('node-telegram-bot-api');

router.post('/sendTelegram', async (res, req) => {
    if (res.body.telegram !== '') {
        const token = res.body.telegram
        const bot = new TelegramBot(token, {polling: true});
        bot.on('message', (msg) => {
            const chatId = msg.chat.id;
            bot.sendMessage(chatId, 'Вы успешно привязали свою группу к сайту');
            return req.status(200).json({message: 'Чат комнаты успешно получен', info: chatId})
        });
    } else {
        req.status(500).json({message: 'Поля обязательно для заполнения'})
    }
})

module.exports = router
